import React from 'react'
import { Container } from 'native-base';

const Layout = (props) => {
    <Container>
        {props.children}
    </Container>

}
import React from 'react'
import { SwiperFlatList } from 'react-native-swiper-flatlist';
import { Card, CardItem } from 'native-base'
import { Image } from 'react-native'

const image = ['1', '2', '3']
const Carousel = ({ data = image }) => {
    return (
        <SwiperFlatList

            data={data}
            renderItem={({ item }) => {
                console.log('item==', item)
                return <Card>
                    <CardItem cardBody>
                        <Image source={require(`../../assets/banner/${item}.jpeg`)} style={{ height: 200, width: 200, flex: 1 }} />
                    </CardItem>
                </Card>
            }}
        />
    )
}
export default Carousel
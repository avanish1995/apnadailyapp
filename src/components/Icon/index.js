import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
const MyIcon = ({ name = 'align-justify', size = 20, color = '#fff', style = {}, onPress }) => <Icon name={name} style={style} onPress={onPress} size={size} color={color} />;
export default MyIcon
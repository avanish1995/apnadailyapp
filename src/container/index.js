import React, { useState } from 'react'
import { Container, Header, Left, Body, Right, Title, Button, Input, Item, Content, Drawer, Card, CardItem, Text } from 'native-base';
import 'react-native-gesture-handler';

import { StyleSheet, TextInput, View } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
// import { Input } from 'react-native-elements';
const Stack = createStackNavigator();

import { widthPercentageToDP as wp, heightPercentageToDP as hp, } from 'react-native-responsive-screen';

import Icon from '../components/Icon'
import Home from '../screens/Home'

const MainContainer = () => {
    const [search, setSearch] = useState('')
    const onSearch = (e) => {
        console.log('e===', e)
        setSearch(e)
    }
    const onClick = () => {
        console.warn('pressed')
        console.log('pressed')
    }
    return (
        <Container >
            <Header searchBar={true} span rounded style={{ backgroundColor: "#0D15A6" }}>
                <Left style={{ width: wp(100), maxWidth: wp(100) }}>
                    <Button transparent>
                        <Icon onPress={() => onClick()} name="align-justify" />
                    </Button>

                </Left>
                <Body style={{ border: 'solid red 1px', justifyContent: 'flex-start' }}>
                    <Item regular style={{ marginLeft: -28, backgroundColor: "white", text: 'center', borderRadius: 10, height: 40, width: wp(80), maxWidth: wp(180), minWidth: wp(80) }}><Icon name='search' color={'#0D15A6'} size={18} style={{ marginLeft: 5 }} /><Input placeholder='Search Products' value={search} onChangeText={onSearch} /></Item>
                </Body>
                {/* <Right> <Button transparent>
                    <Icon onPress={() => onClick()} name="align-justify" />
                </Button> </Right> */}
                <Right>
                    <Button transparent>
                        <Icon name='user-circle' />
                    </Button>

                </Right>
            </Header>
            {/* <Content style={{ marginTop: 20 }}> */}
            {/* <View style={{ flex: 1, flexDirection: 'row', backgroundColor: 'red' }}> */}

            <NavigationContainer >
                <Stack.Navigator initialRouteName="Home" headerMode="none">
                    <Stack.Screen
                        name="Start"
                        component={Home}

                    />
                </Stack.Navigator>
            </NavigationContainer>
            {/* </View> */}
            {/* </Content> */}

        </Container>
    )
}
// const styles = StyleSheet.create({
//     headerContainer: {
//         width: wp(100), padding: 5, flexDirection: 'row'
//     },
//     left: {
//         flex: 1, width: wp(18), flexDirection: 'row', justifyContent: 'space-between'
//     },
//     center: {
//         width: wp(65), alignItems: 'center', justifyContent: 'center'
//     },
//     title: {
//         textAlign: 'center', fontSize: hp(2.8), fontWeight: '600', letterSpacing: 1, color: color.white
//     },
//     right: {
//         flex: 1, width: wp(10), alignItems: 'flex-end', justifyContent: 'center'
//     },
//     subtitle: {
//         marginVertical: 5,
//         textAlign: 'center', fontSize: hp(2.2), fontWeight: '600', letterSpacing: 1, color: color.white
//     }
// })
export default MainContainer
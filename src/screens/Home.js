import React from 'react'
import { Content, Row, Col, Card, CardItem, Body } from 'native-base'
import { View, Text } from 'react-native'
import Carosel from '../components/Carosel'

const Home = () => {
    console.log('home===')
    return (
        <View style={{ flex: 1, flexDirection: 'row', borderColor: 'blue', borderWidth: 2 }}>
            <Content padder={true}>
                <Row>
                    <Carosel />
                </Row>
                <Card style={{ width: 200, height: 200 }}>
                    <CardItem>
                        <Body>
                            <Text style={{ color: 'blue' }}>Hello</Text>
                        </Body>
                    </CardItem>
                </Card>
                <Text style={{ color: 'red' }}>Home</Text>
            </Content>
        </View>
    )
}

export default Home